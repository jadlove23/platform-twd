﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager :Singleton<LevelManager>
{
    public GameObject _startPos;
    public GameObject[] tilePrefabs;
    private point blueSpawn;
    private point redSpawn;
    [SerializeField]
    private GameObject bluePortalPrefab, redPortalPrefab;
    public Portal BluePortal { get; set; }
    public Portal BluePortal1 { get; set; }
    public Portal BluePortal2 { get; set; }
    public Dictionary<point, TileScript> Tiles
    {
        get;set;
    }
    // Start is called before the first frame update
    public float TileSize
    {
        get { return tilePrefabs[0].GetComponent<SpriteRenderer>().sprite.bounds.size.x; }
    }
    void Start()
    {
 
        CreateLevel();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateLevel()
    {
        Tiles = new Dictionary<point, TileScript>() ;
        string[] MapData = new string[]
        {
            "010101010","1111111111","1111111111", "010101010","1111111111","1111111111","01010101010",
        };
        int mapX = MapData[0].ToCharArray().Length;
        int mapY = MapData.Length;
        Vector3 maxTile = Vector3.zero;
        Vector3 wordStart = _startPos.transform.position;
        for (int y=0; y < mapY; y++)
        {
            char[] newtile = MapData[y].ToCharArray();
            for (int x = 0; x < mapX; x++)
            {
                PlaceTile(newtile[x].ToString(), x,y, wordStart);
            }

        }
  
        maxTile = Tiles[new point(mapX - 1, mapY - 1)].transform.position;
        SpawnPortal();
    }
    private void PlaceTile(string tileType, int x, int y, Vector3 wordStart)
    {
        int tileIndex = int.Parse(tileType);
        if(tileIndex == 0)
        {
            TileScript newTile = Instantiate(tilePrefabs[tileIndex]).GetComponent<TileScript>();
            newTile.Setup(new point(x, y), new Vector3(wordStart.x + (TileSize * x), wordStart.y - (TileSize * y), 0));
            Tiles.Add(new point(x, y), newTile);
        }
        

    }
    private void SpawnPortal()
    {
        GameObject tmp = (GameObject)Instantiate(bluePortalPrefab, new Vector3(20,-3.61f), Quaternion.identity);
        GameObject tmp1 = (GameObject)Instantiate(bluePortalPrefab, new Vector3(20, -0.61f), Quaternion.identity);
        GameObject tmp2 = (GameObject)Instantiate(bluePortalPrefab, new Vector3(20, 2.61f), Quaternion.identity);

        BluePortal = tmp.GetComponent<Portal>();
        BluePortal.name = "BluePortal0";

        BluePortal1 = tmp1.GetComponent<Portal>();
        BluePortal1.name = "BluePortal1";

        BluePortal2 = tmp2.GetComponent<Portal>();
        BluePortal2.name = "BluePortal2";


    }
}
