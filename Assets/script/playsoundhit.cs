﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playsoundhit : MonoBehaviour
{
    AudioSource audi;
    // Start is called before the first frame update
    void Start()
    {
        audi = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Monster")
        {
            audi.Play();
        }
    }
}
