﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;

    private Rigidbody2D rb;

    private Touch touch;

    private Vector3 touchPos, whereToMove;

    private bool isMoving = false;
    private Vector2 screenBounds;


    public Button buttonBoom;
    public Transform boomPoint;
    public float attackRang = 0.5f;
    public LayerMask enemyLayers;
    public int BoomDamage;
    public float cooldown = 15;
    private float cooldownTimer;
    private bool boomActive;
    public GameObject boomPrefab;

    private float previousDistanceToTouchPos, currentDistanceToTouchPos;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cooldownTimer = cooldown;
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        
        if (isMoving)
            currentDistanceToTouchPos = (touchPos - transform.position).magnitude;

        if (GameManager.Instance.playerSelect)
        {
            buttonBoom.gameObject.SetActive(true);
            if (Input.touchCount > 0)
            {
                touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    
                    previousDistanceToTouchPos = 0;
                    currentDistanceToTouchPos = 0;
                    if (Camera.main.ScreenToWorldPoint((touch.position)).y >= screenBounds.y * -0.48f)
                    {
                        isMoving = true;
                        touchPos = Camera.main.ScreenToWorldPoint((touch.position));
                        touchPos.z = 0;
                        whereToMove = (touchPos - transform.position).normalized;
                        rb.velocity = new Vector2(whereToMove.x * moveSpeed, whereToMove.y * moveSpeed);
                    }
                    
                }
            }
        }

        if (!GameManager.Instance.playerSelect)
        {
            buttonBoom.gameObject.SetActive(false);
        }


            if (currentDistanceToTouchPos > previousDistanceToTouchPos)
        {
            isMoving = false;
            rb.velocity = Vector2.zero;
        }

        if (isMoving)
        {
            previousDistanceToTouchPos = (touchPos - transform.position).magnitude;
        }

        if (rb.velocity.x > 0 && isMoving)
        {
            this.transform.localScale = new Vector3(-1,1,1);
        }
        if ((rb.velocity.x < 0 && isMoving))
        {
            this.transform.localScale = new Vector3(1,1,1);
        }
        if (boomActive == true)
        {
            cooldownTimer -= Time.deltaTime;
            buttonBoom.interactable = false;
            Debug.Log(cooldownTimer);
            if (cooldownTimer < 0)
            {
                boomActive = false;
                buttonBoom.interactable = true;
                cooldownTimer = cooldown;
            }
        }
    }
    void OnDrawGizmosSelected()
    {
        if (boomPoint == null)
            return;
        Gizmos.DrawWireSphere(boomPoint.position, attackRang);
    }
    
    public void SelectButton()
    {
        GameManager.Instance.playerSelect = !GameManager.Instance.playerSelect;
            Debug.Log(GameManager.Instance.playerSelect);
    }
    public void Boom()
    {
        boomActive = true;
        GameObject bomb = Instantiate(boomPrefab, transform.position,transform.rotation);
        Destroy(bomb, 1f);
        Collider2D[] hitEnemies= Physics2D.OverlapCircleAll(boomPoint.position, attackRang, enemyLayers);
        foreach(Collider2D enemy in hitEnemies)
        {
            Debug.Log("ATTACK!!");
            enemy.GetComponent<Moster>().TakeDamage(BoomDamage);

        }
    }
    
}
