﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class towerBTN : MonoBehaviour
{
    [SerializeField]
    private GameObject towerPrefab;
    [SerializeField]
    private Sprite sprite;
    [SerializeField]
    private int price;
    [SerializeField]
    private Text priceText;
    Vector3 dragStartPos;
    Touch touch;
    public GameObject But;
   


    public GameObject TowerPrefab
    {
        get
        {
            return towerPrefab;
        }
    }

    public Sprite Sprite {
        get
        {
            return sprite;
        }
    }
    private void Start()
    {
        priceText.text = price + "$";
        GameManager.Instance.Changed += new CurrencyChanged(PriceCheck);
    }
    private void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                DragStart();
            }
            if (touch.phase == TouchPhase.Moved)
            {
                Dragging();
            }
            if (touch.phase == TouchPhase.Ended)
            {
                DragRelease();
            }
        }
        
    }
    void DragStart() {
        dragStartPos = Camera.main.ScreenToWorldPoint(touch.position);
    }
    void Dragging() { }
    void DragRelease() { }
    public int Price
    {
        get
        {
            return price;
        }
    }
    public void PriceCheck()
    {
        if(price <= GameManager.Instance.Currency)
        {
            GetComponent<Image>().color = Color.white;
            priceText.color = Color.black;
        }
        else
        {
            GetComponent<Image>().color = Color.grey;
            priceText.color = Color.grey;
        }
    }
   

}
