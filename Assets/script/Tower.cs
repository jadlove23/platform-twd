﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class Tower : MonoBehaviour
{
    [SerializeField]
    private string projectileType;
    [SerializeField]
    private float projectileSpeed;
    private Animator myAnimation;
    AudioSource audioData;
    public int Price
    {
        get;
        set;
    }
    [SerializeField]
    private int damage;
    public float ProjectileSpeed
    {
        get { return projectileSpeed; }
    }
    private SpriteRenderer mySpriteRenderer;
    private Moster target;
    public Moster Target
    {
        get { return target; }
    }

    public int Damage { get { return damage; } }

    private Queue<Moster> monsters = new Queue<Moster>();
    private bool canAttack=true;
    private float attackTimer;
    [SerializeField]
    private float attackCooldown;
    // Start is called before the first frame update
    private void Start()
    {
        audioData = GetComponent<AudioSource>();
    }
    void Awake()
    {
        myAnimation = transform.parent.GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Attack();
        
    }
    public void Select()
    {
        mySpriteRenderer.enabled = !mySpriteRenderer.enabled;
    }
    private void Attack()
    {
        if (!canAttack)
        {
            attackTimer += Time.deltaTime;
            if (attackTimer>= attackCooldown)
            {
                canAttack = true;
                attackTimer = 0;
               
            }
        }
        if (target == null&& monsters.Count>0)
        {
            myAnimation.SetBool("Attack", false);
            target = monsters.Dequeue();
        }
        if(target!=null && target.IsActive)
        {
            if (canAttack)
            {
                Shoot();
                myAnimation.SetBool("Attack", true);
                audioData.Play();
                canAttack = false;
            }
           
        }
    }
    private void Shoot()
    {
        Projectiles projectiles = GameManager.Instance.Pool.GetObject(projectileType).GetComponent<Projectiles>();
        projectiles.transform.position = new Vector3(transform.position.x,transform.position.y+0.3f);
        projectiles.Initialize(this);
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Monster")
        {
            monsters.Enqueue(other.GetComponent<Moster>());
        }
        
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Monster")
        {
            target = null;
            myAnimation.SetBool("Attack", false);
        }
    }
}
