﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AStar 
{
    private static Dictionary<point, Node> nodes;
   private static void CreateNodes()
    {
        nodes = new Dictionary<point, Node>();
        foreach (TileScript tile in LevelManager.Instance.Tiles.Values)
        {
            nodes.Add(tile.Gridposition, new Node(tile));
        }
    }
    public static void GetPath(point start)
    {
        if (nodes == null)
        {
            CreateNodes();
        }
        HashSet<Node> openList = new HashSet<Node>();
        Node currentNode = nodes[start];
        openList.Add(currentNode);
    }
}
