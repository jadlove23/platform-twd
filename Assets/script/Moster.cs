﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moster : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float hp;
    public bool IsActive { get; set; }
    public void Spawn()
    {   if (GameManager.Instance.type == "dragon")
        {
            hp = 500;
            int r = Random.Range(1, 3);
            if (r == 1)
            {
                transform.position = LevelManager.Instance.BluePortal1.transform.position;
            }
            if (r == 2)
            {
                transform.position = LevelManager.Instance.BluePortal2.transform.position;
            }
            StartCoroutine(Scale(new Vector3(0.1f, 0.1f), new Vector3(0.5f, 0.5f), false));
        }
        if (GameManager.Instance.type == "Bat")
        {
            hp = 100;
            int r = Random.Range(1, 3);
            if (r == 1)
            {
                transform.position = LevelManager.Instance.BluePortal1.transform.position;
            }
            if (r == 2)
            {
                transform.position = LevelManager.Instance.BluePortal2.transform.position;
            }
            StartCoroutine(Scale(new Vector3(0.1f, 0.1f), new Vector3(0.3f, 0.3f), false));
        }

        if (GameManager.Instance.type == "Cycop")
        {
            hp = 450;
            transform.position = new Vector3(15f, -2.88f);
            StartCoroutine(Scale(new Vector3(0.1f, 0.1f), new Vector3(1.68f,1.68f), false));
        }
        if (GameManager.Instance.type == "Goblin")
        {
            hp = 120;
            transform.position = new Vector3(15f, -3.94f);
            StartCoroutine(Scale(new Vector3(0.1f, 0.1f), new Vector3(0.5f, 0.5f), false));
        }
          

    }
    public IEnumerator Scale(Vector3 from,Vector3 to,bool remove)
    {
        
        float progres = 0;
        while (progres <= 1)
        {
            transform.localScale = Vector3.Lerp(from, to, progres);
            progres += Time.deltaTime;
            yield return null;
        }
        transform.localScale = to;
        IsActive = true;
        if (remove)
        {
            Release();
        }
    }
    private void Update()
    {
        Move();
    }
    private void Move()
    {
        if (IsActive)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Castle")
        {
            IsActive = false;
            GameManager.Instance.Pool.ReleaseObject(gameObject);
            GameManager.Instance.RemoveMonster(this);
          
            GameManager.Instance.Lives--;
        }
    }
    public void Release()
    {
        IsActive = false;
        GameManager.Instance.Pool.ReleaseObject(gameObject);
        GameManager.Instance.RemoveMonster(this);
    }
    public void TakeDamage(int damage)
    {
        if (IsActive)
        {
            hp -= damage;
            if (hp <= 0)
            {
                if (GameManager.Instance.type == "dragon")
                {
                    GameManager.Instance.Currency += 60;
                    GameManager.Instance.score += 200;
                    Release();
                   
                }
                if (GameManager.Instance.type == "Bat")
                {
                    GameManager.Instance.Currency += 25;
                    GameManager.Instance.score += 100;
                    Release();
                    
                }
                if (GameManager.Instance.type == "Cycop")
                {
                    GameManager.Instance.Currency += 50;
                    GameManager.Instance.score += 150;
                    Release();
                  
                }
                if (GameManager.Instance.type == "Goblin")
                {
                    GameManager.Instance.Currency += 20;
                    GameManager.Instance.score += 100;
                    Release();
                  
                }

            }
        }
        
    }
}
