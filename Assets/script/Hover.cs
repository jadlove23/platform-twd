﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : Singleton<Hover>
{
    private SpriteRenderer spriteRenderer;
    private SpriteRenderer rangeSpriteRenderer;
    Vector3 dragStartPos;
    Touch touch;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(0, -5.53f, 0);
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.rangeSpriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                DragStart();
            }
            if (touch.phase == TouchPhase.Moved)
            {
                Dragging();
            }
            if (touch.phase == TouchPhase.Ended)
            {
                DragRelease();
            }
            

        }
        
        
    }
    void DragStart()
    {
        transform.position = new Vector3(0, -5.53f, 0);
        dragStartPos = Camera.main.ScreenToWorldPoint(touch.position);
        
    }
    void Dragging() {
        
        FollowMouse();  

    }
    void DragRelease() {
       
    }
    private void FollowMouse()
    {
        transform.position = new Vector3(0, -5.53f, 0);
        if (spriteRenderer.enabled)
        { 
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);

        }
        

    }
    public void Activate(Sprite sprite)
    {
        this.spriteRenderer.sprite = sprite;
        spriteRenderer.enabled = true;
        rangeSpriteRenderer.enabled = true;
        transform.position = new Vector3(0, -5.53f, 0);
    }
    public void Deactivate()
    {
        spriteRenderer.enabled = false;
        rangeSpriteRenderer.enabled = false;
        GameManager.Instance.ClickedBtn = null;
    }
}
