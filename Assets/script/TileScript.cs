﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TileScript : MonoBehaviour
{
    public point Gridposition { get; set; }
    public bool IsEmpty { get; set; }

    private Tower myTower;
    private Color32 fullColor= new Color32(255,0,0,255);
    private Color32 emptyColor = new Color32(0, 255, 0, 255);


    private SpriteRenderer spriteRenderer;
    public SpriteRenderer SpriteRenderer { get; set; }
    public bool Debugging { get; set; }

    public Vector2 WorldPossition
    {
        get
        {
            return new Vector2(transform.position.x + (GetComponent<SpriteRenderer>().bounds.size.x/2),transform.position.y-(GetComponent<SpriteRenderer>().bounds.size.y/2));
        }
    }
    Vector3 dragStartPos;
    Touch touch;
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
        }
    }
    public void Setup(point gridPos, Vector3 wordPos)
    {
        IsEmpty = true;
        this.Gridposition = Gridposition;
        transform.position = wordPos;

    }
    private void OnMouseOver()
    {
       
        if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedBtn != null)
        {
            if (IsEmpty&&!Debugging)
            {
                ColorTile(emptyColor);
            }
            if (!IsEmpty && !Debugging )
            {
                ColorTile(fullColor);
                
            }
            if (!IsEmpty && !Debugging&& (Input.GetMouseButtonDown(0)|| touch.phase == TouchPhase.Ended))

            {
                    Hover.Instance.Deactivate();  
            }
            else if (Input.GetMouseButtonDown(0) || touch.phase == TouchPhase.Ended)
            {
                Placetower();
            }
           

        }
       else if (!EventSystem.current.IsPointerOverGameObject()&&GameManager.Instance.ClickedBtn==null&& Input.GetMouseButtonDown(0))
        {
            if (myTower != null)
            {
                GameManager.Instance.SelectTower(myTower);
            }
            else
            {
                GameManager.Instance.DeselectTower();
            }
        }
        
    }
    private void OnMouseExit()
    {
        if (!Debugging)
        {
            ColorTile(Color.white);
        }
       
    }
    public void Placetower()
    {
        
        GameObject tower =(GameObject)Instantiate(GameManager.Instance.ClickedBtn.TowerPrefab, transform.position, Quaternion.identity);
        tower.transform.SetParent(transform);
        this.myTower = tower.transform.GetChild(0).GetComponent<Tower>();
        IsEmpty = false;
        ColorTile(Color.white);
        myTower.Price = GameManager.Instance.ClickedBtn.Price;
        GameManager.Instance.BuyTower();

    }
    private void ColorTile(Color newColor)
    {
        SpriteRenderer.color = newColor;
    }
}
