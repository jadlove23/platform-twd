﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct point 
{
    public int X { get; set; }
    public int Y { get; set; }
    public point(int x,int y)
    {
        this.X = x;
        this.Y = y;
    }
}
