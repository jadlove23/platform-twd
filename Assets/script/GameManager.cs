﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public delegate void CurrencyChanged();
public class GameManager : Singleton<GameManager>
{
    public event CurrencyChanged Changed;
    public towerBTN ClickedBtn { get; set; }
    Touch touch;
    public Text Scoretxt;
    public int score;
    private int currency;
    private int wave = 6;
    private int waveTxt = 0;
    private int lives;

    private bool gameOver = false;

    [SerializeField]
    private Text livesTxt;

    [SerializeField]
    private Text Wavetext;

    [SerializeField]
    private Text moneyTxt;

    [SerializeField]
    private GameObject waveBtn;

    [SerializeField]
    private GameObject gameOverMenu;

    private Tower selectedTower;
    [SerializeField]
    private GameObject sellpanel;
    [SerializeField]
    private Text sellText;
    private List<Moster> activeMonsters = new List<Moster>();
    public string type;
    public bool playerSelect = false;
    public int Currency
    {
        get
        {
            return currency;
        }
        set
        {
            this.currency = value;
            this.moneyTxt.text = value.ToString()+"<color=lime>$</color>";
            OnCurrencyChanged();
        }
    }
    public int Lives
    {
        get { 
            return lives;
            }
        set {
            this.lives = value;

            if (lives <= 0)
            {
                this.lives = 0;
                Gameover();
            }

           
            livesTxt.text = lives.ToString();

            
        }
    }
    public ObjectPool Pool { get; set; }
    public bool WaveActive
    {
        get { return activeMonsters.Count > 0; }
    }
    // Start is called before the first frame update
    private void Awake()
    {
        Pool = GetComponent<ObjectPool>();
    }
    void Start()
    {
        Time.timeScale = 1f;
        score = 0;
        Lives = 10;
        Currency = 300;
    }

    // Update is called once per frame
    void Update()
    {
        Scoretxt.text = "Score:  "+score.ToString();
    }
    public void PickTower(towerBTN  TowerBtn)
    {
        if (Currency >= TowerBtn.Price  && !WaveActive)
        {
            this.ClickedBtn = TowerBtn;
            Hover.Instance.Activate(TowerBtn.Sprite); 
        }
       
    }
    public void BuyTower()
    {
        if(Currency>= ClickedBtn.Price)
        {
            Currency -= ClickedBtn.Price;
            Hover.Instance.Deactivate();
            ClickedBtn = null;    
        }
       
    }
    public void OnCurrencyChanged()
    {
        if (Changed != null)
        {
            Changed();
        }
    }
    public void SelectTower(Tower tower)
    {
        if (selectedTower != null)
        {
            selectedTower.Select();
        }
        selectedTower = tower;
        selectedTower.Select();
        sellText.text = "+" + (selectedTower.Price / 2).ToString();
        sellpanel.SetActive(true);
    }
    public void DeselectTower()
    {
        if (selectedTower != null)
        {
            selectedTower.Select();
        }
        selectedTower = null;
        sellpanel.SetActive(false);
    }
    public void handleEscape()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
            Hover.Instance.Deactivate();   
    }
    public void StartWave()
    {
        wave++;
        waveTxt++;
        Wavetext.text = string.Format("Wave:<color=lime>{0}</color>", waveTxt);
        StartCoroutine(SpawnWave());
        waveBtn.SetActive(false);
    }
    private IEnumerator SpawnWave()
    {

        if (waveTxt <= 2)
        {
            for (int i = 0; i < wave; i++)
            {
                int monsterIndex = Random.Range(0, 2);
                Debug.Log(monsterIndex);
                type = string.Empty;
                switch (monsterIndex)
                {
                    case 0:
                        type = "Bat";
                        break;
                    case 1:
                        type = "Goblin";

                        break;
                }
                Moster monster = Pool.GetObject(type).GetComponent<Moster>();
                monster.Spawn();
                activeMonsters.Add(monster);
                yield return new WaitForSeconds(2.5f);
            }
        }
        if(waveTxt > 2 && waveTxt < 5)
        {
            for (int i = 0; i < wave; i++)
            {
                int monsterIndex = Random.Range(0, 3);
                Debug.Log(monsterIndex);
                type = string.Empty;
                switch (monsterIndex)
                {
                    case 0:
                        type = "Bat";
                        break;
                    case 1:
                        type = "Goblin";

                        break;
                    case 2:
                        type = "Cycop";

                        break;
                }
                Moster monster = Pool.GetObject(type).GetComponent<Moster>();
                monster.Spawn();
                activeMonsters.Add(monster);
                yield return new WaitForSeconds(2.5f);
            }

        }
        if(waveTxt >= 5 & waveTxt < 8) 
        {
            for (int i = 0; i < wave; i++)
            {

                int monsterIndex = Random.Range(0, 4);
                Debug.Log(monsterIndex);
                type = string.Empty;
                switch (monsterIndex)
                {
                    case 0:
                        type = "Bat";
                        break;
                    case 1:
                        type = "Goblin";

                        break;
                    case 2:
                        type = "Cycop";

                        break;
                    case 3:
                        type = "dragon";
                        break;
                }
                Moster monster = Pool.GetObject(type).GetComponent<Moster>();
                monster.Spawn();
                activeMonsters.Add(monster);
                yield return new WaitForSeconds(2.5f);
            }

        }
        if (waveTxt >= 8 & waveTxt < 10)
        {
            for (int i = 0; i < wave; i++)
            {

                int monsterIndex = Random.Range(0, 4);
                Debug.Log(monsterIndex);
                type = string.Empty;
                switch (monsterIndex)
                {
                    case 0:
                        type = "Bat";
                        break;
                    case 1:
                        type = "Goblin";

                        break;
                    case 2:
                        type = "Cycop";

                        break;
                    case 3:
                        type = "dragon";
                        break;
                }
                Moster monster = Pool.GetObject(type).GetComponent<Moster>();
                monster.Spawn();
                activeMonsters.Add(monster);
                yield return new WaitForSeconds(1.25f);
            }

        }
        if (waveTxt >=10)
        {
            for (int i = 0; i < wave+5; i++)
            {

                int monsterIndex = Random.Range(0, 4);
                Debug.Log(monsterIndex);
                type = string.Empty;
                switch (monsterIndex)
                {
                    case 0:
                        type = "Bat";
                        break;
                    case 1:
                        type = "Goblin";

                        break;
                    case 2:
                        type = "Cycop";

                        break;
                    case 3:
                        type = "dragon";
                        break;
                }
                Moster monster = Pool.GetObject(type).GetComponent<Moster>();
                monster.Spawn();
                activeMonsters.Add(monster);
                yield return new WaitForSeconds(0.5f);
            }

        }

    }
    public void RemoveMonster(Moster monster)
    {
        activeMonsters.Remove(monster);
        if (!WaveActive && !gameOver)
        {
            waveBtn.SetActive(true);
        }
    }
    public void Gameover()
    {
        if (!gameOver)
        {
            Time.timeScale = 0;
            gameOver = true;
            gameOverMenu.SetActive(true);
        }
    }
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainGame");
    }
    public void QuitGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void SellTower()
    {
        if (selectedTower != null)
        {
            Currency += selectedTower.Price / 2;
            selectedTower.GetComponentInParent<TileScript>().IsEmpty = true;
            Destroy(selectedTower.transform.parent.gameObject);
            DeselectTower();
        }
    }
}
