﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class MainMenuControlScriptBGMSFX : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button buttonStart;
    [SerializeField] Button buttonHowtoplay;
    [SerializeField] Button buttonCredit;
    [SerializeField] Button buttonOptions;
    [SerializeField] Button buttonExit;

    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource>();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];
        SetupButtonsDelegate();
        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop();
        audiosourceButtonUI.PlayOneShot(audioclipHoldOver);
    }
    void SetupButtonsDelegate()
    {
        buttonStart.onClick.AddListener(delegate { StartButtonClick(buttonStart); });
        buttonHowtoplay.onClick.AddListener(delegate { HowtoplayButtonClick(buttonOptions); });
        buttonCredit.onClick.AddListener(delegate { CreditButtonClick(buttonOptions); });
        buttonOptions.onClick.AddListener(delegate { OptionsButtonClick(buttonOptions); });
        buttonExit.onClick.AddListener(delegate { ExitButtonClick(buttonExit); });


    }
    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("MainGame");
    }
    public void OptionsButtonClick(Button button)
    {
        if (!SingletonGameApplicationManager.Instance.IsOptionMenuActive)
        {
            SceneManager.LoadScene("OptionScene", LoadSceneMode.Additive);
            SingletonGameApplicationManager.Instance.IsOptionMenuActive = true;
        }
    }
    public void CreditButtonClick(Button button)
    {
           SceneManager.LoadScene("CreditScene");
    }
    public void HowtoplayButtonClick(Button button)
    {
            SceneManager.LoadScene("Howtoplay", LoadSceneMode.Additive);
    }
    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }

}
