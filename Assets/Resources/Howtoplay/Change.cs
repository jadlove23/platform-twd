﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Change : MonoBehaviour
{
    public Sprite[] s1;
    public Button b1;
    int count = 0;
    [SerializeField] Button _backButton;
    void Awake()
    {
    s1 = Resources.LoadAll<Sprite>("Howtoplay");
    }
   public void On_Click_Button(){
       count++;
       if(count == s1.Length){
           count=0;
       }
       b1.image.sprite = s1[count];
   
   }
    private void Start()
    {
        _backButton.onClick.AddListener(delegate { BackButtonClick(_backButton); });
    }
    public void BackButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("Howtoplay");
       
    }
}